import csv
import math
import pandas as pd
import numpy as np
import sys

from collections import Counter
from itertools import chain, combinations

from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KernelDensity
from sklearn.feature_selection import mutual_info_classif
from sklearn import svm
from sklearn.metrics import roc_auc_score
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import mutual_info_regression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import preprocessing
from sklearn.utils import (as_float_array, check_array, check_X_y, safe_sqr,safe_mask)
from sklearn.utils.extmath import safe_sparse_dot, row_norms
from sklearn.utils.validation import check_is_fitted
from sklearn.preprocessing import LabelBinarizer

from scipy import special, stats
from scipy.sparse import issparse
from scipy.stats import chi2_contingency
from scipy.stats import chisquare

from functions import *

if(len(sys.argv) < 5):
	print("Usage: main.py file_name label_column_name date_time_column_name percent_of_features_to_select")
	sys.exit()
else:
	file_name = sys.argv[1]
	label_col = sys.argv[2]
	date_col = sys.argv[3]
	fp = sys.argv[4]


# Load and split data
data = parse_data(file_name, label_col)
data1, data2, data3 = prepare_data(data, date_col)


data_train_y = data1['label'].append(data2['label'])
data_train = data1.append(data2)
data_train = data_train.drop('label',axis=1)


data_test_y = data3['label']

columns = list(data_train)
fCnt = math.ceil(len(list(data_train)) * int(fp)/100) # count of features to select


# select features using IGR
ig, igr = info_gain(data_train.copy(), data_train_y)
sortedIg = [i[0] for i in sorted(enumerate(igr), key=lambda x:x[1], reverse=True)]
self = []
for i in range(fCnt):
	self.append(columns[sortedIg[i]])

data_train_selected = data_train[self]
data_test_selected = data3[self]
clf = DecisionTreeClassifier(random_state=0)

clf.fit(data_train_selected, data_train_y)
scores = clf.predict(data_test_selected)
print("--------------------------------------------")
print("Feature set selected by igr method:")
print(self)
print("Classifier AUC:", roc_auc_score(data_test_y, scores))

# ----------------------------------------------------
# ----------------------------------------------------
for method in [chi2_my, mutual_info_classif]:
	selected = SelectKBest(method, k=int(fCnt)).fit(data_train, data_train_y).get_support()
	self = []
	for col, sel in zip(columns, selected):
		if(sel == True):
			self.append(col)

	data_train_selected = data_train[self]
	data_test_selected = data3[self]
	clf = DecisionTreeClassifier(random_state=0)
    
	clf.fit(data_train_selected, data_train_y)
	scores = clf.predict(data_test_selected)
	print("--------------------------------------------")
	print("Feature set selected by", method.__name__, "method:")
	print(self)
	print("Classifier AUC:", roc_auc_score(data_test_y, scores))

# select features using proposed method

self = select_features(data1, data2, fp)

data_train_cd_selected = data_train[self]
data_test_cd = data3[self]

clf = DecisionTreeClassifier(random_state=0)
clf.fit(data_train_cd_selected, data_train_y)
scores_cd = clf.predict(data_test_cd)
print("--------------------------------------------")
print("Feature set selected by proposed method:")
print(self)
print("Classifier AUC:", roc_auc_score(data_test_y, scores_cd))


# brute force feature selection
bestSet, score = bf_select(data_train, data_train_y, data3.drop('label', axis=1), data3['label'], int(fCnt))
print("--------------------------------------------")
print("Feature set selected by brute force:")
print(bestSet)
print("Classifier AUC:", score)
