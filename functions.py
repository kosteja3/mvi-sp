import csv
import math
import pandas as pd
import numpy as np

from sklearn.naive_bayes import MultinomialNB
from collections import Counter
from sklearn.neighbors import KernelDensity
from sklearn.feature_selection import mutual_info_classif
from sklearn import svm
from sklearn.metrics import roc_auc_score
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import mutual_info_regression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import preprocessing
from itertools import chain, combinations
from sklearn.utils import (as_float_array, check_array, check_X_y, safe_sqr,safe_mask)
from sklearn.utils.extmath import safe_sparse_dot, row_norms
from sklearn.utils.validation import check_is_fitted
from sklearn.preprocessing import LabelBinarizer

from scipy import special, stats
from scipy.sparse import issparse
from scipy.stats import chi2_contingency
from scipy.stats import chisquare


# parse data and do some pro proccesing
# preproceesing valid only for aut+mpg data set
def parse_data(path, label_col):
    df = pd.read_csv(path, delimiter=",", quotechar='"')
    df.replace(to_replace='?',value=np.nan,inplace=True)
    df.dropna(axis=0, inplace=True)
    df.columns = ['label' if x==label_col else x for x in df.columns]
    for col in list(df):
        if(df[col].dtype == 'object'):
            df[col] = df[col].astype('category')
            df[col] = df[col].cat.codes
    
    return df
def parse_data_1(path):
    df = pd.read_csv(path, delimiter=',', quotechar='"')
    return df

# sort data by date/time column and split
def prepare_data(data, time_col):
    data = data.sort_values(time_col, kind='mergesort')
    data = data.drop([time_col], axis=1)
    cnt = data.shape[0]
    size = cnt // 3
    data1 = data.iloc[:size]
    data2 = data.iloc[size+1:2*size]
    data3 = data.iloc[2*size+1:cnt]
    return data1, data2, data3

# implementation of addtive smoothing
def laplace_smoothing(X, alpha, unique_vals):
    N = X.shape[0] # count of all values
    d = unique_vals.shape[0] # count of all distinct values
    cnt = Counter(X)
    prob = []
    for xi in unique_vals:
        prob.append([xi, (cnt[xi] + alpha)/(N + d*alpha)])
    return prob

# jaccard similarity between two variables (should work both for discrete and continous)
def jaccard_sim(a, b):
    sum_min = 0;
    sum_max = 0
    for i in range(len(a)):
        sum_min = sum_min + min(a[i], b[i])
        sum_max = sum_max + max(a[i], b[i])
    return sum_min/sum_max

def sim(x, y):
    a = []
    b = []
    for i in x:
        a.append(i[1])
    for i in y:
        b.append(i[1])
    return jaccard_sim(a, b)

# jaccard similarity between two gaussians
def sim_gauss(kde1, kde2, data1, data2):
    minVal = min(min(data1), min(data2))
    maxVal = max(max(data1), max(data2))
    step = (maxVal - minVal) / 1000
    a = []
    b = []
    for i in range(1000):
        a.append(pow(2, kde1.score(minVal + i*step)))
        b.append(pow(2, kde2.score(minVal + i*step)))
    return jaccard_sim(a, b)

# similatrity between continous variables
def sim_cont(data1, data2, col):
    X_p_1 = data1.loc[data1['label'] == 1][col]
    X_n_1 = data1.loc[data1['label'] == 0][col]
    
    X_p_2 = data2.loc[data2['label'] == 1][col]
    X_n_2 = data2.loc[data2['label'] == 0][col]
    
    kde1_p = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(X_p_1.values.reshape(-1, 1))
    kde2_p = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(X_p_2.values.reshape(-1, 1))
    
    kde1_n = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(X_n_1.values.reshape(-1, 1))
    kde2_n = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(X_n_2.values.reshape(-1, 1))
    
    p_class = sim_gauss(kde1_p, kde2_p, X_p_1, X_p_2)
    n_class = sim_gauss(kde1_n, kde2_n, X_n_1, X_n_2)
    
    p_class_cnt = X_p_1.shape[0] + X_p_2.shape[0]
    n_class_cnt = X_n_1.shape[0] + X_n_2.shape[0]
    
    return (p_class * p_class_cnt + n_class * n_class_cnt) / (p_class_cnt + n_class_cnt)
   
# similarity between discrete variables
def sim_disc(data1, data2, col):
    
    data12 = data1[col].append(data2[col])
    unique_vals = np.unique(data12)
    
    X_p_1 = data1.loc[data1['label'] == 1][col]
    X_n_1 = data1.loc[data1['label'] == 0][col]
    prob_p_1 = laplace_smoothing(X_p_1, 1, unique_vals)
    prob_n_1 = laplace_smoothing(X_n_1, 1, unique_vals)

    X_p_2 = data2.loc[data2['label'] == 1][col]
    X_n_2 = data2.loc[data2['label'] == 0][col]
    prob_p_2 = laplace_smoothing(X_p_2, 1, unique_vals)
    prob_n_2 = laplace_smoothing(X_n_2, 1, unique_vals)
    
    p_class = sim(prob_p_1, prob_p_2) # similarity between positive clasess
    n_class = sim(prob_n_1, prob_n_2) # similartiy between negative classes
    
    p_class_cnt = X_p_1.shape[0] + X_p_2.shape[0] # count of postive samples
    n_class_cnt = X_n_1.shape[0] + X_n_2.shape[0] # count of negative samples
    
    return (p_class * p_class_cnt + n_class * n_class_cnt) / (p_class_cnt + n_class_cnt)
    

# counts concept drift index
def concept_drift_value(data1, data2, col):
    if(data1[col].dtypes == 'int64'): 
        return sim_disc(data1, data2, col) # discrete feature
    else:
        return sim_cont(data1, data2, col) # continous feature

# selects best p% features sorted by the value of concept_drift_value*info_gain
def select_features(data1, data2, p):
    data = data1.append(data2)
    data_label = data['label']
    data = data.drop(['label'], axis=1)
    columns = list(data)
    
    ig = mutual_info_classif(data, data_label)
    
    cdi = []
    for col in columns:
        cdi.append(concept_drift_value(data1, data2, col))
    w = [a*b for a,b in zip(ig, cdi)]
    sel_f = []
    for id,i in enumerate(columns):
        sel_f.append([i, w[id]])
    fCnt = math.ceil(len(columns) * (int(p)/100))
    sel_f = sorted(sel_f, key=lambda x: x[1], reverse=True)[:fCnt]
    selected_features = []
    for i in sel_f:
        selected_features.append(i[0])
    return selected_features

# my own implemenation of chi2 test
def chi2_my(d, labels):
    d = pd.DataFrame(d)
    cols = list(d)
    d['label'] = labels.reshape(-1,1)
    chi2_vals = []
    chi2_ps = []

    for col in cols:
        if(d[col].dtype != "int64"):
            h, bins = np.histogram(d[col])
            pos_tmp, bins = np.histogram(d[d['label']==1][col], bins=bins)
            neg_tmp, bins = np.histogram(d[d['label']==0][col], bins=bins)
            pos_table = []
            neg_table = []
            for i,j in zip(pos_tmp, neg_tmp):
                if(i != 0 or j != 0):
                    pos_table.append(i)
                    neg_table.append(j)
        else:

            pos = d[d['label']==1][col].value_counts()
            neg = d[d['label']==0][col].value_counts()
            bins = d[col].unique()
            dict1 = dict(zip(bins, [0]*len(bins)))
            dict2 = dict(zip(bins, [0]*len(bins)))

            for key,val in pos.items():
                dict1[key] = val

            for key,val in neg.items():
                dict2[key] = val

            pos_table = []
            neg_table = []

            for i in bins:
                pos_table.append(dict1[i])
                neg_table.append(dict2[i])

        cont_t = [pos_table, neg_table]
        chi2_val, a, b, c = chi2_contingency(cont_t)
        chi2_vals.append(chi2_val)
        chi2_ps.append(a)
    return [chi2_vals, chi2_ps]

def entropy(x):
    """
    x is assumed to be an (nsignals, nsamples) array containing integers between
    0 and n_unique_vals
    """
    x = np.atleast_2d(x)
    nrows, ncols = x.shape
    nbins = x.max() + 1

    # count the number of occurrences for each unique integer between 0 and x.max()
    # in each row of x
    counts = np.vstack((np.bincount(row, minlength=nbins) for row in x))

    # divide by number of columns to get the probability of each unique value
    p = counts / float(ncols)
    # compute Shannon entropy in bits
    p = p[np.where(p > 0.000001)]
    return -np.sum(p * np.log(p))

def info_gain(data, y):
    ig = []
    igr = []
    data['label'] = y
    cols = list(data.drop('label', axis=1))
    for c in cols:
        sum_i = 0
        sum_intr = 0
        if(data[c].dtype != "int64"):
            h, bins = np.histogram(data[c])
            pos_tmp, bins = np.histogram(data[data['label']==1][c], bins=bins)
            neg_tmp, bins = np.histogram(data[data['label']==0][c], bins=bins)
            
            bin_tmp = [sum(x) for x in zip(pos_tmp, neg_tmp)]
            bin_vals = []
            for i in bin_tmp:
                if (i > 0):
                    bin_vals.append(i)
                    
            bin_cnt = len(bin_vals)
            labels = []
            for i,j in zip(pos_tmp, neg_tmp):
                if(i > 0 or j > 0):
                    l = []
                    for ii in range(i):
                        l.append(1)
                    for jj in range(j):
                        l.append(0)
                    labels.append(l)

            cnt = len(data[c])
          
            for i in range(bin_cnt):
                value_cnt = bin_vals[i]
                entr = entropy(labels[i])
                sum_i += (value_cnt/cnt)*entr
                sum_intr += (value_cnt/cnt)*np.log((value_cnt/cnt))
        else:
            bins = data[c].unique()
            cnt = len(data[c])
            for b in bins:
                d = pd.DataFrame(data[data[c] == b][[c, 'label']])
                entr = entropy(d['label'])
                sum_i += (len(d)/cnt)*entr
                sum_intr += (len(d)/cnt)*np.log((len(d)/cnt))

        ig_val=entropy(data['label']) - sum_i
        igr.append(ig_val/-sum_intr)
        ig.append(entropy(data['label']) - sum_i)
    return ig, igr

# train_data, train_data_labels, test_data, test_data_labels, count_of_features_to_select
def bf_select(data, y, data_test, y_test, fCnt):
    cols = list(data)
    comb = set(combinations(cols, fCnt))
    
    scoreBest = 0
    setBest = list(comb)[0]
    for c in comb:
        sel = np.array(c)
        clf = DecisionTreeClassifier(random_state=0)
        clf.fit(data[sel], y)
        predict = clf.predict(data_test[sel]) 
        score = roc_auc_score(y_test, predict)
        if(score > scoreBest):
            scoreBest = score 
            setBest = sel
    return setBest, scoreBest

